<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'sent' => 'Cek inbox di email anda, kami telah mengirim link untuk reset password anda!',
    'user' => "Kami tidak menemukan pengguna dengan email tersebut.",
    'reset' => 'Reset password berhasil!',
    'password' => 'Sandi harus minimal 6 karakter dan cocok dengan konfirmasi password.',
    'token' => 'Token reset password tidak valid.',

];
